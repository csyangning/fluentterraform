from fluent_terraform import FluentTerraform
from const import Provider, Region, Zone, NetworkTier, IpAddressType, DiskType, DiskInterface, InstanceType, ImageType, NicType

def main():
  test = FluentTerraform()\
    .add_provider(Provider.GOOGLE_BETA)\
      .set_project('ningy-cloud') \
    .add_instance('vm_instance')\
      .set_name('terraform-instance')\
      .add_boot_disk()\
        .set_image(ImageType.Debian10)\
      .add_network_interface()\
        .set_network_name('default')\

  test.to_config('min_config')

if __name__ == "__main__":
    main()