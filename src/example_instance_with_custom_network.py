from fluent_terraform import FluentTerraform
from const import Provider, Region, Zone, NetworkTier, IpAddressType, DiskType, DiskInterface, InstanceType, ImageType, NicType

def main():
  test = FluentTerraform()\
    .add_provider(Provider.GOOGLE_BETA)\
      .set_project('ningy-cloud') \
      .set_region(Region.us_central1) \
      .set_zone(Zone.us_central1_c)\
    .add_network('test_network')\
      .set_provider(Provider.GOOGLE_BETA)\
      .set_name('terraform-test-network')\
      .set_description('test network')\
      .set_mtu(1500)\
      .create_subnetwork(True)\
    .add_instance('vm_instance')\
      .set_provider(Provider.GOOGLE_BETA)\
      .set_name('terraform-instance')\
      .set_description('test instance')\
      .set_machine_type(InstanceType.e2_medium)\
      .set_zone(Zone.us_central1_c)\
      .set_enable_display(True)\
      .add_tag('test')\
      .add_tag('dev')\
      .add_boot_disk()\
        .set_image(ImageType.Debian10)\
        .set_type(DiskType.pd_ssd)\
      .add_network_interface()\
        .set_network_name('test_network', is_resource=True)\
        .set_nic_type(NicType.VIRTIO_NET)

  test.to_config('instance_with_custom_network')

if __name__ == "__main__":
    main()