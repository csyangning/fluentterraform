from enum import Enum

class Provider(Enum):
  GOOGLE = 1
  GOOGLE_BETA = 2
  AWS = 3
  Azure = 4

class NetworkTier(Enum):
  STANDARD = 1
  PREMIUM = 2

class DiskType(Enum):
  pd_standard = 1
  pd_balanced = 2
  pd_ssd = 3

class DiskInterface(Enum):
  SCSI = 1
  NVME = 2

class InstanceType(Enum):
  e2_medium = 1

class ImageType(Enum):
  Debian10 = 1
  Ubuntu = 2
  Cos = 3

class Region(Enum):
  us_central1 = 1

class Zone(Enum):
  us_central1_c = 1

class IpAddressType(Enum):
  EXTERNAL = 1
  INTERNAL = 2

class NicType(Enum):
  VIRTIO_NET = 1
  GVNIC = 2