from fluent_terraform import FluentTerraform
from const import Provider, Region, Zone, NetworkTier, IpAddressType, DiskType, DiskInterface, InstanceType, ImageType, NicType

def main():
  test = FluentTerraform()\
    .add_provider(Provider.GOOGLE_BETA)\
      .set_project('ningy-cloud') \
      .set_region(Region.us_central1) \
      .set_zone(Zone.us_central1_c)\
    .add_ip_address('vm_test_static_ip')\
      .set_provider(Provider.GOOGLE_BETA)\
      .set_name('terraform-static-ip')\
      .set_description('test static ip')\
      .set_network_tier(NetworkTier.PREMIUM)\
      .set_address_type(IpAddressType.EXTERNAL)\
    .add_instance('vm_instance')\
      .set_provider(Provider.GOOGLE_BETA)\
      .set_name('terraform-instance')\
      .set_description('test instance')\
      .set_machine_type(InstanceType.e2_medium)\
      .set_zone(Zone.us_central1_c)\
      .set_enable_display(True)\
      .add_tag('test')\
      .add_tag('dev')\
      .add_boot_disk()\
        .set_image(ImageType.Debian10)\
        .set_type(DiskType.pd_ssd)\
      .add_network_interface()\
        .set_network_name('default')\
        .set_nic_type(NicType.VIRTIO_NET)\
        .set_nat_ip('vm_test_static_ip')

  test.to_config('instance_with_static_ip')

if __name__ == "__main__":
    main()