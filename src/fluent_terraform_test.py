import unittest
import textwrap

from fluent_terraform import FluentTerraform
from const import Provider, Region, Zone, NetworkTier, IpAddressType, DiskType, DiskInterface, InstanceType, ImageType, NicType

class TestStringMethods(unittest.TestCase):
  def test_provider(self):
    test = FluentTerraform().add_provider(Provider.GOOGLE)\
      .set_project('ningy-cloud') \
      .set_region(Region.us_central1) \
      .set_zone(Zone.us_central1_c)
    expect = textwrap.dedent(
      '''
      provider "google" {
        project = "ningy-cloud"
        region  = "us-central1"
        zone    = "us-central1-c"
      }
      ''')
    self.assertEqual(test.to_config(), expect)

  def test_network(self):
    test = FluentTerraform().add_network('vpc_network')\
      .set_name('terraform-network')\
      .set_description('test network')\
      .set_mtu(1500)\
      .create_subnetwork(True)
    expect = textwrap.dedent(
      '''
      resource "google_compute_network" "vpc_network" {
        provider = google
        name = "terraform-network"
        description = "test network"
        auto_create_subnetworks = "true"
        mtu = 1500
      }
      ''')
    self.assertEqual(test.to_config(), expect)

  def test_multi_resources(self):
    test = FluentTerraform()\
      .add_provider(Provider.GOOGLE)\
        .set_project('ningy-cloud') \
        .set_region(Region.us_central1) \
        .set_zone(Zone.us_central1_c) \
      .add_network('vpc_network')\
        .set_name('terraform-network')\
        .set_description('test network')\
        .set_mtu(1500)\
        .create_subnetwork(True)
    expect = textwrap.dedent(
      '''
      provider "google" {
        project = "ningy-cloud"
        region  = "us-central1"
        zone    = "us-central1-c"
      }
      
      resource "google_compute_network" "vpc_network" {
        provider = google
        name = "terraform-network"
        description = "test network"
        auto_create_subnetworks = "true"
        mtu = 1500
      }
      ''')
    self.assertEqual(test.to_config(), expect)

  def test_ip_address(self):
    test = FluentTerraform().add_ip_address('vm_static_ip')\
      .set_name('terraform-static-ip')\
      .set_description('test static ip')\
      .set_network_tier(NetworkTier.STANDARD)\
      .set_address_type(IpAddressType.INTERNAL)
    expect = textwrap.dedent(
      '''
      resource "google_compute_address" "vm_static_ip" {
        provider = google
        name = "terraform-static-ip"
        description = "test static ip"
        address_type = "INTERNAL"
        network_tier = "STANDARD"
      }
      ''')
    self.assertEqual(test.to_config(), expect)

  def test_disk(self):
    test = FluentTerraform().add_disk('vm_disk')\
      .set_name('test-disk')\
      .set_description('test blank disk')\
      .set_type(DiskType.pd_ssd)\
      .set_zone(Zone.us_central1_c)\
      .set_size_in_gb(5)\
      .set_interface(DiskInterface.NVME)\
      .add_label(('env', 'dev'))\
      .add_label(('purpose', 'test'))
    expect = textwrap.dedent(
      '''
      resource "google_compute_disk" "vm_disk" {
        provider = google
        name = "test-disk"
        description = "test blank disk"
        type = "pd-ssd"
        zone = "us-central1-c"
        size = 5
        labels = {
          env = "dev"
          purpose = "test"
        }
      }
      ''')
    self.assertEqual(test.to_config(), expect)

  def test_instance_with_default_network(self):
    test = FluentTerraform().add_instance('vm_instance')\
      .set_provider(Provider.GOOGLE)\
      .set_name('terraform-instance')\
      .set_description('test instance')\
      .set_machine_type(InstanceType.e2_medium)\
      .set_zone(Zone.us_central1_c)\
      .set_enable_display(True)\
      .add_tag('test')\
      .add_tag('dev')\
      .add_boot_disk()\
        .set_image(ImageType.Debian10)\
        .set_type(DiskType.pd_ssd)\
      .add_network_interface()\
        .set_network_name('default')\
        .set_nic_type(NicType.GVNIC)
      
    expect = textwrap.dedent(
      '''
      resource "google_compute_instance" "vm_instance" {
        provider = google
        name = "terraform-instance"
        description = "test instance"
        machine_type = "e2-medium"
        zone = "us-central1-c"
        tags = ["test", "dev"]
        enable_display = "true"
        boot_disk {
          initialize_params {
            image = "debian-cloud/debian-9"
            type = "pd-ssd"
          }
        }
        network_interface {
          network = "default"
          nic_type = "GVNIC"
          access_config {

          }
        }

      }
      ''')
    self.assertEqual(test.to_config(), expect)

  def test_instance_with_extra_disk(self):
    self.maxDiff = 1000
    test = FluentTerraform().add_instance('vm_instance')\
      .set_provider(Provider.GOOGLE)\
      .set_name('terraform-instance')\
      .set_description('test instance')\
      .set_machine_type(InstanceType.e2_medium)\
      .set_zone(Zone.us_central1_c)\
      .set_enable_display(True)\
      .add_tag('test')\
      .add_tag('dev')\
      .add_extra_disk('test_disk_1')\
      .add_extra_disk('test_disk_2')\
      .add_boot_disk()\
        .set_image(ImageType.Debian10)\
        .set_type(DiskType.pd_ssd)\
      .add_network_interface()\
        .set_network_name('default')\
        .set_nic_type(NicType.GVNIC)
      
    expect = textwrap.dedent(
      '''
      resource "google_compute_instance" "vm_instance" {
        provider = google
        name = "terraform-instance"
        description = "test instance"
        machine_type = "e2-medium"
        zone = "us-central1-c"
        tags = ["test", "dev"]
        enable_display = "true"
        boot_disk {
          initialize_params {
            image = "debian-cloud/debian-9"
            type = "pd-ssd"
          }
        }
        network_interface {
          network = "default"
          nic_type = "GVNIC"
          access_config {

          }
        }
        attached_disk {
          source = google_compute_disk.test_disk_1.name
        }
        attached_disk {
          source = google_compute_disk.test_disk_2.name
        }
      }
      ''')
    self.assertEqual(test.to_config(), expect)

  def test_instance_with_static_ip(self):
    test = FluentTerraform().add_instance('vm_instance')\
      .set_provider(Provider.GOOGLE)\
      .set_name('terraform-instance')\
      .set_description('test instance')\
      .set_machine_type(InstanceType.e2_medium)\
      .set_zone(Zone.us_central1_c)\
      .set_enable_display(True)\
      .add_tag('test')\
      .add_tag('dev')\
      .add_boot_disk()\
        .set_image(ImageType.Debian10)\
        .set_type(DiskType.pd_ssd)\
      .add_network_interface()\
        .set_network_name('default')\
        .set_nic_type(NicType.GVNIC)\
        .set_nat_ip('test_ip')
      
    expect = textwrap.dedent(
      '''
      resource "google_compute_instance" "vm_instance" {
        provider = google
        name = "terraform-instance"
        description = "test instance"
        machine_type = "e2-medium"
        zone = "us-central1-c"
        tags = ["test", "dev"]
        enable_display = "true"
        boot_disk {
          initialize_params {
            image = "debian-cloud/debian-9"
            type = "pd-ssd"
          }
        }
        network_interface {
          network = "default"
          nic_type = "GVNIC"
          access_config {
            nat_ip = google_compute_address.test_ip.address
          }
        }

      }
      ''')
    self.assertEqual(test.to_config(), expect)

  def test_instance_with_network_resource(self):
    test = FluentTerraform().add_instance('vm_instance')\
      .set_provider(Provider.GOOGLE)\
      .set_name('terraform-instance')\
      .set_description('test instance')\
      .set_machine_type(InstanceType.e2_medium)\
      .set_zone(Zone.us_central1_c)\
      .set_enable_display(True)\
      .add_tag('test')\
      .add_tag('dev')\
      .add_boot_disk()\
        .set_image(ImageType.Debian10)\
        .set_type(DiskType.pd_ssd)\
      .add_network_interface()\
        .set_network_name('test_network', is_resource=True)\
        .set_nic_type(NicType.GVNIC)
      
    expect = textwrap.dedent(
      '''
      resource "google_compute_instance" "vm_instance" {
        provider = google
        name = "terraform-instance"
        description = "test instance"
        machine_type = "e2-medium"
        zone = "us-central1-c"
        tags = ["test", "dev"]
        enable_display = "true"
        boot_disk {
          initialize_params {
            image = "debian-cloud/debian-9"
            type = "pd-ssd"
          }
        }
        network_interface {
          network = google_compute_network.test_network.name
          nic_type = "GVNIC"
          access_config {

          }
        }

      }
      ''')
    self.assertEqual(test.to_config(), expect)

if __name__ == '__main__':
  unittest.main()