from __future__ import annotations
import textwrap
import os

from const import Provider, Region, Zone, NetworkTier, IpAddressType, DiskType, DiskInterface, InstanceType, ImageType, NicType


class FluentFlowInterface(object):
  def add_provider(self, provider: Provider) -> ProviderNode:
    raise NotImplementedError()

  def add_instance(self, name: str) -> InstanceNode:
    raise NotImplementedError()

  def add_disk(self, name: str) -> DiskNode:
    raise NotImplementedError()

  def add_network(self, name: str) -> NetworkNode:
    raise NotImplementedError()

  def add_ip_address(self, name: str) -> IpAddressNode:
    raise NotImplementedError()

  def to_config(self, folder: str = None) -> str:
    raise NotImplementedError()

class ResourceNode(FluentFlowInterface):
  def __init__(self, engine: FluentTerraform):
    self.engine = engine

  def add_provider(self, provider: Provider) -> ProviderNode:
    return self.engine.add_provider(provider)

  def add_instance(self, name: str) -> InstanceNode:
    return self.engine.add_instance(name)

  def add_disk(self, name: str) -> DiskNode:
    return self.engine.add_disk(name)

  def add_network(self, name: str) -> NetworkNode:
    return self.engine.add_network(name)

  def add_ip_address(self, name: str) -> IpAddressNode:
    return self.engine.add_ip_address(name)

  def to_string(self) -> str:
    raise NotImplementedError()

  def to_config(self, folder: str = None) -> str:
    return self.engine.to_config(folder)

class FluentTerraform(FluentFlowInterface):
  def __init__(self):
    self.nodes = []

  def add_provider(self, provider: Provider) -> ProviderNode:
    provider = ProviderNode(self, provider)
    self.nodes.append(provider)
    return provider

  def add_instance(self, name: str) -> InstanceNode:
    instance = InstanceNode(self, name)
    self.nodes.append(instance)
    return instance

  def add_disk(self, name: str) -> DiskNode:
    disk = DiskNode(self, name)
    self.nodes.append(disk)
    return disk

  def add_network(self, tag_name: str) -> NetworkNode:
    network = NetworkNode(self, tag_name)
    self.nodes.append(network)
    return network

  def add_ip_address(self, name: str) -> IpAddressNode:
    ip = IpAddressNode(self, name)
    self.nodes.append(ip)
    return ip

  def to_config(self, folder: str = None) -> str:
    result = ''
    for node in self.nodes:
      result += node.to_string()

    if folder:
      if not os.path.exists(folder):
        os.makedirs(folder)
      target_file = os.path.join(folder, 'main.tf')
      print('Writing tf file to: ' + target_file)
      with open(target_file, 'w') as f:
        f.write(result)

    return result

class ProviderNode(ResourceNode):
  def __init__(self, engine: FluentTerraform, provider: Provider):
    super().__init__(engine)
    self.provider = provider
    self.project = None
    self.region = Region.us_central1
    self.zone = Zone.us_central1_c

  def set_project(self, project: str) -> ProviderNode:
    self.project = project
    return self

  def set_region(self, region: Region) -> ProviderNode:
    self.region = region
    return self

  def set_zone(self, zone: Zone) -> ProviderNode:
    self.zone = zone
    return self

  def to_string(self) -> str:
    assert self.project
    assert self.region
    assert self.zone

    if self.provider not in [Provider.GOOGLE, Provider.GOOGLE_BETA]:
      raise NotImplementedError('only gcp is supported')

    config = textwrap.dedent(
        '''
      provider "{provider}" {{
        project = "{project}"
        region  = "{region}"
        zone    = "{zone}"
      }}
      '''
        .format(
            provider=self.provider.name.lower().replace('_', '-'),
            project=self.project,
            region=self.region.name.replace('_', '-'),
            zone=self.zone.name.replace('_', '-')))

    return config

class CloudResourceBase(ResourceNode):
  RESOURCE_TYPE = None

  def __init__(self, engine: FluentTerraform, tag_name: str):
    self.tag_name = tag_name
    self.engine = engine
    self.name = None
    self.description = None
    self.provider = Provider.GOOGLE

  def set_name(self, name: str) -> CloudResourceBase:
    self.name = name
    return self

  def set_description(self, description: str) -> CloudResourceBase:
    self.description = description
    return self

  def set_provider(self, provider: Provider) -> CloudResourceBase:
    self.provider = provider
    return self

  def to_string(self) -> str:
    self.name
    return ''


class NetworkNode(CloudResourceBase):
  """
  refernce: https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_network
  """
  RESOURCE_TYPE = 'google_compute_network'

  def __init__(self, engine: FluentTerraform, tag_name: str):
    super().__init__(engine, tag_name)
    self.auto_create_subnetworks = True
    self.mtu = 1460

  def create_subnetwork(self, enable: bool) -> NetworkNode:
    self.auto_create_subnetworks = enable
    return self

  def set_mtu(self, mtu: int) -> NetworkNode:
    self.mtu = mtu
    return self

  def to_string(self) -> str:
    base = super().to_string()

    config = textwrap.dedent(
      '''
      resource "{type}" "{tag_name}" {{
        provider = {provider}
        name = "{name}"
        description = "{description}"
        auto_create_subnetworks = "{auto_create_subnetworks}"
        mtu = {mtu}
      }}
      '''
        .format(
            provider=self.provider.name.lower().replace('_', '-'),
            type=self.RESOURCE_TYPE,
            tag_name=self.tag_name,
            description=self.description,
            name=self.name,
            auto_create_subnetworks=str(self.auto_create_subnetworks).lower(),
            mtu=self.mtu))

    return base + config

class IpAddressNode(CloudResourceBase):
  """
  Reference: https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address
  """
  RESOURCE_TYPE = 'google_compute_address'

  def __init__(self, engine: FluentTerraform, tag_name: str):
      super().__init__(engine, tag_name)
      self.address_type = IpAddressType.EXTERNAL
      self.network_tier = NetworkTier.PREMIUM

  def set_address_type(self, type: IpAddressType) -> IpAddressNode:
    self.address_type = type
    return self

  def set_network_tier(self, network_tier: NetworkTier) -> IpAddressNode:
    self.network_tier = network_tier
    return self

  def to_string(self) -> str:
    base = super().to_string()

    config = textwrap.dedent(
      '''
      resource "{type}" "{tag_name}" {{
        provider = {provider}
        name = "{name}"
        description = "{description}"
        address_type = "{address_type}"
        network_tier = "{network_tier}"
      }}
      '''
        .format(
            provider=self.provider.name.lower().replace('_', '-'),
            type=self.RESOURCE_TYPE,
            tag_name=self.tag_name,
            description=self.description,
            name=self.name,
            address_type =self.address_type.name,
            network_tier=self.network_tier.name))

    return base + config

class DiskNode(CloudResourceBase):
  """
  reference: https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk
  """
  RESOURCE_TYPE = 'google_compute_disk'

  def __init__(self, engine: FluentTerraform, tag_name: str):
      super().__init__(engine, tag_name)
      self.disk_type = DiskType.pd_balanced
      self.zone = Zone.us_central1_c
      self.size_in_gb = 10
      self.interface = DiskInterface.SCSI
      self.labes = []

  def set_type(self, type: DiskType) -> DiskNode:
    self.disk_type = type
    return self

  def set_zone(self, zone: Zone) -> DiskNode:
    self.zone = zone
    return self

  def set_size_in_gb(self, size: int) -> DiskNode:
    self.size_in_gb = size
    return self

  def add_label(self, label: (str, str)) -> DiskNode:
    self.labes.append(label)
    return self

  def set_interface(self, interface: DiskInterface) -> DiskNode:
    self.interface = interface
    return self

  def to_string(self) -> str:
    base = super().to_string()

    labels = ''
    for label in self.labes:
      labels += '{0} = "{1}"\n          '.format(label[0], label[1])

    config = textwrap.dedent(
      '''
      resource "{type}" "{tag_name}" {{
        provider = {provider}
        name = "{name}"
        description = "{description}"
        type = "{disk_type}"
        zone = "{zone}"
        size = {size}
        labels = {{
          {labels}
        }}
      }}
      '''
        .format(
            provider=self.provider.name.lower().replace('_', '-'),
            type=self.RESOURCE_TYPE,
            tag_name=self.tag_name,
            description=self.description,
            name=self.name,
            disk_type=self.disk_type.name.replace('_', '-'),
            zone=self.zone.name.replace('_', '-'),
            size=self.size_in_gb,
            interface=self.interface.name,
            labels=labels.rstrip()))

    return base + config

class InstanceNode(CloudResourceBase):
  """
  Reference: https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance
  """

  RESOURCE_TYPE = 'google_compute_instance'

  def __init__(self, engine: FluentTerraform, tag_name: str):
      super().__init__(engine, tag_name)
      self.machine_type = InstanceType.e2_medium
      self.zone = Zone.us_central1_c
      self.tags = []
      self.enable_display = False
      self.boot_disk = None
      self.network_interface = None
      self.extra_disks = []

      # The instance is using some beta features
      self.provider = Provider.GOOGLE_BETA

  def set_machine_type(self, machine_type: InstanceType) -> InstanceNode:
    self.machine_type = machine_type
    return self

  def set_zone(self, zone: Zone) -> InstanceNode:
    self.zone = zone
    return self

  def add_tag(self, tag: str) -> InstanceNode:
    self.tags.append(tag)
    return self

  def set_enable_display(self, enable: bool) -> InstanceNode:
    self.enable_display = enable
    return self

  def add_boot_disk(self) -> BootDiskNode:
    self.boot_disk = BootDiskNode(self)
    return self.boot_disk

  def add_network_interface(self) -> NetworkInterfaceNode:
    self.network_interface = NetworkInterfaceNode(self)
    return self.network_interface

  def add_extra_disk(self, disk_tag_name:str) -> InstanceNode:
    self.extra_disks.append('google_compute_disk.{}.name'.format(disk_tag_name))
    return self

  def to_string(self):
    base = super().to_string()
    assert self.boot_disk
    assert self.network_interface

    tags = ''
    for tag in self.tags:
      tags += '"{}", '.format(tag)
    tags = tags.rstrip(', ')

    extra_disk = ''
    for disk_str in self.extra_disks:
      extra_disk += textwrap.dedent('''
      attached_disk {{
        source = {}
      }}'''.format(disk_str))

    # fix some str format
    boot_disk = '\n'.join(
      [' ' * 8 + line for line in self.boot_disk.to_string().split('\n')])

    network_interface = '\n'.join(
      [' ' * 8 + line for line in self.network_interface.to_string().split('\n')])
      
    extra_disk = '\n'.join(
      [' ' * 8 + line for line in extra_disk.split('\n')])

    config = textwrap.dedent(
      '''
      resource "{type}" "{tag_name}" {{
        provider = {provider}
        name = "{name}"
        description = "{description}"
        machine_type = "{machine_type}"
        zone = "{zone}"
        tags = [{tags}]
        enable_display = "{enable_display}"
        {boot_disk}
        {network_interface}
        {extra_disk}
      }}
      '''
        .format(
            provider=self.provider.name.lower().replace('_', '-'),
            type=self.RESOURCE_TYPE,
            tag_name=self.tag_name,
            name=self.name,
            description=self.description,
            machine_type=self.machine_type.name.replace('_', '-'),
            zone=self.zone.name.replace('_', '-'),
            tags=tags,
            enable_display=str(self.enable_display).lower(),
            boot_disk=boot_disk.strip(),
            network_interface=network_interface.strip(),
            extra_disk=extra_disk.strip()))

    return base + config

class InstanceNestedResourceNode(FluentFlowInterface):
  def __init__(self, instance: InstanceNode):
    self.instance = instance
  
  def add_boot_disk(self) -> BootDiskNode:
    return self.instance.add_boot_disk()

  def add_network_interface(self) -> NetworkInterfaceNode:
    return self.instance.add_network_interface()

  # FluentFlowInterface
  def add_provider(self, provider: Provider) -> ProviderNode:
    return self.instance.add_provider(provider)

  def add_instance(self, name: str) -> InstanceNode:
    return self.instance.add_instance(name)

  def add_disk(self, name: str) -> DiskNode:
    return self.instance.add_disk(name)

  def add_network(self, name: str) -> NetworkNode:
    return self.instance.add_network(name)

  def add_ip_address(self, name: str) -> IpAddressNode:
    return self.instance.add_ip_address(name)

  def to_string(self) -> str:
    raise NotImplementedError()

  def to_config(self, folder: str = None) -> str:
    return self.instance.to_config(folder)

class BootDiskNode(InstanceNestedResourceNode):

  image_type_string_map = {
    ImageType.Debian10: 'debian-cloud/debian-9'
  }

  def __init__(self, instance: InstanceNode):
    super().__init__(instance)
    self.image = ImageType.Debian10
    self.disk_type = DiskType.pd_balanced

  def set_image(self, image: ImageType) -> BootDiskNode:
    self.image = image
    return self

  def set_type(self, type: DiskType) -> BootDiskNode:
    self.disk_type = type
    return self

  def to_string(self) -> str:
    config = textwrap.dedent(
      '''
      boot_disk {{
        initialize_params {{
          image = "{image}"
          type = "{disk_type}"
        }}
      }}
      '''
        .format(
            image=self.image_type_string_map[self.image],
            disk_type=self.disk_type.name.replace('_', '-')
        ))
    return config

class NetworkInterfaceNode(InstanceNestedResourceNode):

  def __init__(self, instance: InstanceNode):
    super().__init__(instance)
    self.network_name = None
    self.disk_type = DiskType.pd_balanced
    self.nic_type = NicType.VIRTIO_NET
    self.nat_ip = ''

  def set_network_name(self, network_name: str, is_resource: bool = False) -> NetworkInterfaceNode:
    if is_resource:
      self.network_name = 'google_compute_network.{}.name'.format(network_name)
    else:
      self.network_name = '"{}"'.format(network_name)
    return self 

  def set_nic_type(self, nic_type: NicType) -> NetworkInterfaceNode:
    self.nic_type = nic_type
    return self

  def set_nat_ip(self, ip_tag_name: str) -> NetworkInterfaceNode:
    self.nat_ip = 'nat_ip = google_compute_address.{}.address'.format(ip_tag_name)
    return self

  def to_string(self) -> str:
    assert self.network_name
    config = textwrap.dedent(
      '''
      network_interface {{
        network = {network_name}
        nic_type = "{nic_type}"
        access_config {{
          {nat_ip}
        }}
      }}
      '''
        .format(
            network_name=self.network_name,
            nic_type=self.nic_type.name,
            nat_ip=self.nat_ip
        ))
    return config
