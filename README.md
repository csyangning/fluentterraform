# FluentTerraform

- [Proposal](https://docs.google.com/document/d/1thO01SNz0lo0UDgLD9H_S_WI9ilQcUGGha3ROTXDdRw/edit?usp=sharing)
- [Design](https://docs.google.com/document/d/1bghwchA9BngIQ2usEmhusIsXPlQdKvOH06MeTEGzKaw/edit?usp=sharing)
- [Report](https://docs.google.com/document/d/1jALl285AssH0hwown6w_AsAkchLOxLDeOV4X7itQVjk/edit?usp=sharing)
- [Poster](https://docs.google.com/presentation/d/1J6d5jJgA59BLi-h5s2WS7gI77Iyahj9GcXdyIQQxiYQ/edit?usp=sharing)
- Demo: See `demo.mov` or youtube [link](https://www.youtube.com/watch?v=wPC1sw12X_c)

## The language
```python
  FluentTerraform()\
   .add_provider(Provider.XXX)\
   # Set/Add Provider Properties
   .add_[resource_type]\
   # Set/Add Resource type properties
   # ...
     .add_[nested_resource]\
     # Set/Add Resource type properties
     # ...
   # ...
   .to_config([output_file_path])
```

## Examples

Example library:

- simple instance: `src/example_simple_instance`
- min config instance: `src/example_min_config.py`
- instance with customer network: `src/example_instance_with_custom_network.py`
- instance with static ip: `src/example_instance_with_static_ip.py`
- instance with extra disk: `src/example_instance_with_extra_disk.py`
- instance with all options: `src/example_instance_with_all_options.py`

How to run the example:

1. Go to `src` folder
2. Run `python [example.py]`
3. It will create a folder match the example name, go to the folder
4. You can fild a file called `main.tf`
5. Run `terraform init` and `terraform apply`

## Run All Test
`python src/fluent_terraform_test.py`

## Design

![](classes.png?raw=true)